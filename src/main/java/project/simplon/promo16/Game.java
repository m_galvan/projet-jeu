package project.simplon.promo16;

public class Game {
    private boolean isEnd = false;
    private String currPlayer = "joueur1";
    private int turn = 1;
    private int moves = 1;
    private int row, column, check = 0;
    Player player = new Player();

    /**
     * Permet de faire tourner le jeu.
     */
    public void game() {
        // Scanner input = new Scanner(System.in);
        Grid grid = new Grid();

        do {
            player.playerSymbolChoice();
            player.initPlayersGrids();
            grid.displayGrid();
            while (moves < 10 && !player.isVictory()) {
                check = 0;
                System.out.println("\nAu tour du " + currPlayer);

                do {
                    if (check != 0) {
                        System.out.print("\nIl y a deja un symbole a cette emplacement\n");
                    }
                    do {
                        System.out.print("Entrer la ligne: ");
                        row = player.getInput().nextInt();

                        System.out.print("Entrer la colonne: ");
                        column = player.getInput().nextInt();

                        if (row > 3 || row <= 0 || column > 3 || column <= 0) {
                            System.out.print("\nLes lignes et les colonnes sont comprises entre 1 et 3\n");
                        }

                    } while (row > 3 || row <= 0 || column > 3 || column <= 0);
                    check++;

                } while (!grid.getCases()[row - 1][column - 1].equals(" "));

                movesInGrid(grid, player);
                movesInPlayersTables(player);

                grid.displayGrid();

                player.victoryConditions(this.currPlayer, "joueur1", player.getPlayer1(), player.getSymbolPl1());
                player.victoryConditions(this.currPlayer, "joueur2", player.getPlayer2(), player.getSymbolPl2());

                turn++;
                playerTurn();

                moves++;
            }
            victory();
        } while (isEnd);
        player.getInput().close();
    }

    /**
     * Permet l'affichage du symbole du joueur actuel.
     * @param grid
     * @param player
     */
    public void movesInGrid(Grid grid, Player player) {
        if (currPlayer.equals("joueur1")) {
            grid.getCases()[row - 1][column - 1] = player.getSymbolPl1();
        }
        if (currPlayer.equals("joueur2")) {
            grid.getCases()[row - 1][column - 1] = player.getSymbolPl2();
        }
    }

    /**
     * Ajoute le symbole du joueur actuel dans sa grille.
     * @param player
     */
    public void movesInPlayersTables(Player player) {
        if (currPlayer.equals("joueur1")) {
            player.getPlayer1()[row - 1][column - 1] = player.getSymbolPl1();
        } else if (currPlayer.equals("joueur2")) {
            player.getPlayer2()[row - 1][column - 1] = player.getSymbolPl2();
        }
    }

    /**
     * Permet de passer d'un joueur à un autre.
     */
    public void playerTurn() {
        if (turn % 2 == 0) {
            currPlayer = "joueur2";
        } else {
            currPlayer = "joueur1";
        }
    }

    /**
     * Annnonce la victoire ou la nulle.
     */
    public void victory() {
        if (player.isVictory() == false) {
            System.out.print("\nMatch nul\n");
        } else if (player.isVictory() == true) {
            System.out.print("\nLe vainqueur est: " + player.getWinner() + "\n");
        }
    }
}
