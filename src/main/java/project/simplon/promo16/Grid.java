package project.simplon.promo16;

public class Grid {
    private String[][] cases = new String[3][3];

    /**
     * Affiche la grille
     */
    public void displayGrid() {
        System.out.print("\n");
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                if (cases[i][j] == null) {
                    cases[i][j] = " ";
                }
                System.out.print("     ");
                if (j == 0) {
                    System.out.print(cases[i][j] + "     |\t");
                }
                if (j == 1) {
                    System.out.print(cases[i][j] + "        |\t");
                }
                if (j == 2) {
                    System.out.print(cases[i][j] + "\t");
                }
                if (j == 2 && i < 2) {
                    System.out.print("\n___________|__________________|______________\n");
                }
                if (i == 2 && j == 2) {
                    System.out.println("\n           |                  |              \n");
                }
            }
        }
        System.out.print("\n");
    }

    public String[][] getCases() {
        return cases;
    }
}
