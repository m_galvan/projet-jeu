package project.simplon.promo16;

import java.util.Scanner;

public class Player {
    private String[][] player1 = new String[3][3];
    private String[][] player2 = new String[3][3];
    private String symbolChoice = "";
    private String symbolPl1;
    private String symbolPl2 = "";
    private boolean victory = false;
    private String winner = "";
    private Scanner input = new Scanner(System.in);

    /**
     * Permet le choix du joueur.
     */
    public void playerSymbolChoice() {

        while (!(symbolChoice.toUpperCase().equals("X") || symbolChoice.toUpperCase().equals("O"))) {
            System.out.print("Joueur 1 choissisez symbole soit X soit O: ");
            symbolChoice = input.nextLine();
        }

        symbolPl1 = symbolChoice.toUpperCase();

        if (symbolPl1.equals("O")) {
            symbolPl2 = "X";
        }
        if (symbolPl1.equals("X")) {
            symbolPl2 = "O";
        }
    }

    public void initPlayersGrids() {
        for (int i = 0; i <= 2; i++) {
            for (int j = 0; j <= 2; j++) {
                player1[i][j] = "";
                player2[i][j] = "";
            }
        }
    }

    /**
     * Test les conditions de victoire.
     *
     * @param currPlayer
     * @param playerNb
     * @param player
     * @param symbolPlayer
     */
    public void victoryConditions(String currPlayer, String playerNb, String[][] player, String symbolPlayer) {
        if (currPlayer.equals(playerNb)) {
            if (player[0][0].equals(symbolPlayer) && player[0][1].equals(symbolPlayer)
                    && player[0][2].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[1][0].equals(symbolPlayer) && player[1][1].equals(symbolPlayer)
                    && player[1][2].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[2][0].equals(symbolPlayer) && player[2][1].equals(symbolPlayer)
                    && player[2][2].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[0][0].equals(symbolPlayer) && player[1][1].equals(symbolPlayer)
                    && player[2][2].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[0][2].equals(symbolPlayer) && player[1][1].equals(symbolPlayer)
                    && player[2][0].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[0][0].equals(symbolPlayer) && player[1][0].equals(symbolPlayer)
                    && player[2][0].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[0][1].equals(symbolPlayer) && player[1][1].equals(symbolPlayer)
                    && player[2][1].equals(symbolPlayer)) {
                victory = true;
            }

            if (player[0][2].equals(symbolPlayer) && player[1][2].equals(symbolPlayer)
                    && player[2][2].equals(symbolPlayer)) {
                victory = true;
            }
        }

        if (victory == true) {
            winner = currPlayer;
        }
    }

    public String[][] getPlayer1() {
        return player1;
    }

    public String[][] getPlayer2() {
        return player2;
    }

    public String getSymbolPl1() {
        return symbolPl1;
    }

    public String getSymbolPl2() {
        return symbolPl2;
    }

    public boolean isVictory() {
        return victory;
    }

    public String getWinner() {
        return winner;
    }

    public Scanner getInput() {
        return input;
    }
}
