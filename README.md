# Projet jeu

<p>
    <img src="https://i.imgur.com/3SmHMB5.png" width="300">
</p>

### Description

Un jeu de Morpion en console (lignes par colonnes) que j'ai classé pour me familiariser avec la POO en Java.

### Les classes

* **Grid**
    *   La méthode displayGrid() affiche la grille.
    <img src="https://i.imgur.com/4js3V2o.png" width="450">
    <br>

* **Player** (Permet de gérer les possibilités des joueurs)
    *   victoryConditions test toutes les conditions de victoire des joueurs.
     <img src="https://i.imgur.com/AgcLljq.png" width="450">
    <br>

* **Game** (Permet au jeu de tourner)<br>
    *   La méthode movesInGrid() permet l'affichage des symboles dans la grille et movesInPlayersTables() ajoute les coups des joueurs dans leur tableau respectif.
     <img src="https://i.imgur.com/rmfRsOi.png" width="450">
